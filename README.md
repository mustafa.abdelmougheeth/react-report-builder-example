# React Report Builder example

This project is an usage example of [react-report-builder](https://www.npmjs.com/package/react-report-builder)

React report builder is an easy to install component set for adding it to existing solutions in order to provide to ad-hoc querying of the data to customers.

Configuration of component can be done via TypeScript example code at [`app/index.tsx`](app/index.tsx). 

The example use Demo Peekdata Reporting API at https://demo.peekdata.io:8443/datagateway/rest/v1.

Features of the component:

- Getting Metadata of Data sources, Dimensions and Metrics
- Flexile building of requests to Peekdata Reporting Data API Engine
- Customization of request via Filtering, Sorting and additional options
- Getting CURL and request example
- Getting formed SQL Query
- Getting the data in different formats (JSON, CSV) 
- Showing data using different views


Usage:

- Clone this repository
- Install dependencies by running `npm install`
- Execute command `npm run start` (it builds the example and runs it at http://localhost:3000/)

After successful compilation, example can be tested via [react-report-builder](https://www.npmjs.com/package/react-report-builder) or [CURL calls](https://gitlab.com/peekdata/datagateway-config-examples/-/tree/master/northwind).


To make example available (not through localhost only) or change the default port, set host to '0.0.0.0' at line devServer: { host: '0.0.0.0', port: 3000 } at file webpack.config.js.


More [documentation](https://peekdata.io/developers/index.html#/react-report-builder/)

